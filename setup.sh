#!/bin/bash

read -p "Enter Platform for Dev [linux|windows|osx] : " platform
case $platform in
        linux | osx | windows)
                echo "Building env for $platform"
                ;;
        *)
                echo "Invalid platform. only linux|osx|windows are supported"
                exit 1
                ;;
esac

cd gdnative || exit
git clone --recursive https://github.com/godotengine/godot-cpp -b 3.x
godot3 --gdnative-generate-json-api api.json
cd godot-cpp
scons platform="$platform" generate_bindings=yes bits=64 -j4 # can be windows, linux or osx
cd ../.. || exit
cd ui
touch project.godot
cd ..

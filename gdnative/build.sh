#!/bin/bash
#
# The following is a demo of using clang++ instead of scons
clang++ -fPIC -c src/*.cpp -g -O3 -std=c++14 -Igodot-cpp/include -Igodot-cpp/include/core -Igodot-cpp/include/gen -Igodot-cpp/godot-headers # build our library
clang++ -v -o ../ui/include/x11/gdexample.so -shared libgdexample.o -Lgodot-cpp/bin -Lgodot-cpp/bin/libgodot-cpp.linux.64.a # Link it
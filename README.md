# godot-program-template

Template used to build complete application using Godot 3.x as a UI engine. 
**NOTE: This makes use of GDNative, not the newer GDExtensions** 

# Initialization
Run `setup.sh` and enter the current OS architecture to allow it to build the gdnative dev environment.



# Making C++ Library

1. C++ libraries for Godot can easily be built all in `gdnative/src`. By default, this template contains the following files;

    - `gdlibrary.cpp`: The following is core to how GDNative finds libraries as such, it is **essential to keep this file** but to change the class registered and the header when a new library is built.
    - `gdexample.h`: The following is the header file used as part of our demo library it **can be removed**
    - `gdexample.cpp`: The following is a demo library code, and **can be removed**

2. Once we have our CPP files all in place, we can build it using SCons. But before then, it is crucial to edit `gdnative/SContruct` to output a correctly named file.
    - `gdexample`: The name of the fully built CPP Library

4. To build the library we now simply run `scons platform=<dev_platform> target=release` replace `<dev_platform>` with `windows, linux or osx`

3. The Library will now be built automatically inside of the `ui/include/` folder. As such, it can be linked directly by Godot.

4. Godot now needs a `GDNativeLibrary`, which denotes to Godot what dynamic Library we want to load for what platform. As well as a `NativeScript` which we can use to denote what class to load from the associated Library. Both of these can be done directly through the 
[Godot UI](https://gamedevadventures.posthaven.com/using-c-plus-plus-and-gdnative-in-godot-part-1)

# Godot UI

1. To link the newly made Godot library into GDScript its simply a matter of specifying the correct `NativeScript` to load ex: `var gdexample = load("res://include/gdexample.gdns").new()`. Now all registered methods can be called on `gdexample`.


## Themeing

1. Godot does have an excellent theming interface allowing you to tune and change any and all widgets with ease manually. An example of a premade theme [By Mounir Tohami](https://mounirtohami.itch.io/godot-dark-theme)

2. Extract the archive into `ui/theme`, find the parent control node and open its theme property, drag the `.theme` file to the theme property, and it will be applied to all its children and sub children


# License

The following codebase is licensed entirely under [Share don't Sell License](https://gitlab.com/SparrowOchon/show-dont-sell)
. PLEASE READ CAREFULLY.
